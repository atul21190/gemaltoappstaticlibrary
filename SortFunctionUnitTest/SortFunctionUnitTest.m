//
//  SortFunctionUnitTest.m
//  SortFunctionUnitTest
//
//  Created by Atul Kumar on 6/18/19.
//  Copyright © 2019 Atul Kumar. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <SortFunctionStaticLibrary/SortFunction.h>

@interface SortFunctionUnitTest : XCTestCase

@end

@implementation SortFunctionUnitTest

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testSortedFunction {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    float array[3] =  {1.0,3.0,2.0};

      mergeSort(array, 0, 2);
    NSMutableArray *ctoObjectiveCArray = [[NSMutableArray alloc] init];

    for (int i = 0; i < 3; ++i) {
        [ctoObjectiveCArray addObject:[NSNumber numberWithFloat:array[i]]];
        
    }
    NSMutableArray *floatArray = [[NSMutableArray alloc] init];
    [floatArray addObject:[NSNumber numberWithFloat:3.0]];
    [floatArray addObject:[NSNumber numberWithFloat:2.0]];
    [floatArray addObject:[NSNumber numberWithFloat:1.0]];

    XCTAssertTrue([floatArray isEqual:ctoObjectiveCArray], @"The objects should be equal");

  //  XCTAssertEqualObjects(floatArray,@[1,2,3]);
    
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
