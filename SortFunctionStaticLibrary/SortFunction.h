//
//  SortFunction.h
//  SortFunctionStaticLibrary
//
//  Created by Atul Kumar on 6/15/19.
//  Copyright © 2019 Atul Kumar. All rights reserved.
//

#ifndef SortFunction_h
#define SortFunction_h

#include <stdio.h>
void mergeSort(float arr[], int l, int r);

#endif /* SortFunction_h */
